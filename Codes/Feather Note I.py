from tkinter import *
from tkinter.filedialog import *
from tkinter.scrolledtext import ScrolledText
import tkinter.messagebox
import itchat
import time
import calendar

class Btn_def():
    def load(self,contents):
        try:
            global filename
            filename=askopenfilename(filetype=[('All Files','*.*')],initialdir='C:\\Desktop\\',initialfile='Enter Filename',parent=top,title='Open')
            with open(filename) as file:
                contents.delete('1.0',END)
                contents.insert(INSERT,file.read())
        except FileNotFoundError:
            pass
    
    def save_as(self,contents):
        try:
            global filename
            filename=asksaveasfilename(filetype=[('All Files','*.*')],initialdir='C:\\Desktop\\',initialfile='Enter Filename',parent=top,title='Save&Save As')
            with open(filename,'w') as file:
                file.write(contents.get('1.0',END))
        except FileNotFoundError:
            pass
        else:
            tkinter.messagebox.showinfo(title='Information',message='Save as file successfully.')
        
    def save(self,contents):
        try:
            global filename
            with open(filename,'w') as file:
                file.write(contents.get('1.0',END))
        except FileNotFoundError:
            pass

class Functions:
    def about():
        try:
            about=Tk()
            about.title('About')
            about.geometry('220x220')

            introduction=Label(about,text='Feather Note I\nby JackHou2005 and TigerZhang2005\n2019.9.28 updata',bg='#FFFFE0',width=30,height=30)
            introduction.pack(side=TOP)
        except:
            pass

    def calendar_function():
        try:
            years=int(time.strftime('%Y',time.localtime()))
            months=int(time.strftime('%m',time.localtime()))
            calendar_data=calendar.month(years,months)

            calendar_window=Tk()
            calendar_window.title('Calendar')
            calendar_window.geometry('230x280')

            build_in_calendar=Label(calendar_window,text=calendar_data,bg='#FFFFE0',font=(3),width=23,height=23,justify=LEFT)
            build_in_calendar.pack(side=TOP)
        except:
            pass

    def notepad():
        try:
            notepad=Tk()
            notepad.title('Notepad')
            notepad.geometry('220x300')

            contents=ScrolledText(notepad,bg='#FFFFE0')
            contents.pack(side=BOTTOM,expand=True,fill=BOTH)
        except:
            pass

    def share_to_wx():
        try:
            global filename
            filename=askopenfilename(filetype=[('All Files','*.*')],initialdir='C:\\Desktop\\',initialfile='Enter Filename',parent=top,title='Open')
            with open(filename) as file:
                text=file.read()
            itchat.auto_login(hotReload=True)
            itchat.send(filename+'\n'+text,toUserName='filehelper')
        except:
            pass
        else:
            tkinter.messagebox.showinfo(title='Information',message='File delivered successfully.')

    def trickit():
        try:
            currentTime=time.strftime('Clock:%Y-%m-%d, %I(%H):%M:%S, %A',time.localtime(time.time()))
            clock.config(text=currentTime)
            top.update()
            clock.after(1000,trickit)
        except:
            pass

top=Tk()
top.title("Feather Note I")

Btn=Btn_def()

clock=Label(top,text='Clock:'+time.strftime('%Y-%m-%d, %I(%H):%M:%S, %A',time.localtime(time.time())))
clock.after(1000,Functions.trickit)
clock.pack(side=BOTTOM)

contents=ScrolledText(top,bg='#FFFFE0')
contents.pack(side=BOTTOM,expand=True,fill=BOTH)

menubar=Menu(top)

filemenu=Menu(menubar,tearoff=0)
menubar.add_cascade(label='File',menu=filemenu)
filemenu.add_command(label='Open',command=lambda:Btn.load(contents))
filemenu.add_command(label='Save As',command=lambda:Btn.save_as(contents))
filemenu.add_command(label='Save',command=lambda:Btn.save(contents))

toolmenu=Menu(menubar,tearoff=0)
menubar.add_cascade(label='Tools',menu=toolmenu)
toolmenu.add_command(label='Share To WeChat',command=Functions.share_to_wx)
toolmenu.add_command(label='Notepad',command=Functions.notepad)
toolmenu.add_command(label='Calendar',command=Functions.calendar_function)

aboutmenu=Menu(menubar,tearoff=0)
menubar.add_cascade(label='About',menu=aboutmenu)
aboutmenu.add_command(label='About',command=Functions.about)

top.config(menu=menubar)

top.mainloop()
