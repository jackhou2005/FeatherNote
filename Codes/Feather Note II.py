from tkinter import *
import time
from tkinter.scrolledtext import ScrolledText
from tkinter.filedialog import *

class Btn_def():
    def save_as(self,contents,notes,summary):
        try:
            global filename
            filename=asksaveasfilename(filetype=[('All Files','*.*')],initialdir='C:\\Desktop',initialfile='Enter Filename',parent=top,title='Save As')
            with open(filename,'w') as file:
                file.write(contents.get('1.0',END))
            with open(filename+' Evidence','w') as file:
                file.write(notes.get('1.0',END))
            with open(filename+' Summary','w') as file:
                file.write(summary.get('1.0',END))
            tkinter.messagebox.showerror(title='Information',message='The file has been save as')
        except FileNotFoundError:
            pass

    def save(self,contents,notes,summary):
        try:
            global filename
            with open(filename,'w') as file:
                file.write(contents.get('1.0',END))
            with open(filename+' Evidence','w') as file:
                file.write(notes.get('1.0',END))
            with open(filename+' Summary','w') as file:
                file.write(summary.get('1.0',END))
            tkinter.messagebox.showinfo(title='Information',message='The file has been save')
        except FileNotFoundError:
            tkinter.messagebox.showinfo(title='Information',message='Cannot save the file')
            pass

    def load(self,contents,notes,summary):
        try:
            global filename
            filename=askopenfilename(filetype=[('All Files','*.*')],initialdir='C:\\Desktop',initialfile='Enter Filename',parent=top,title='Open')
            with open(filename) as file:
                contents.delete('1.0',END)
                contents.insert(INSERT,file.read())
            with open(filename+' Evidence') as file:
                notes.delete('1.0',END)
                notes.insert(INSERT,file.read())
            with open(filename+' Summary') as file:
                summary.delete('1.0',END)
                summary.insert(INSERT,file.read())
        except FileNotFoundError:
            tkinter.messagebox.showerror(title='Information',message='Cannot open the file')
            pass

def trickit():
    currentTime=time.strftime('Clock:%Y-%m-%d, %I(%H):%M:%S, %A',time.localtime(time.time()))
    clock.config(text=currentTime)
    top.update()
    clock.after(1000,trickit)

def about():
    about=Tk()
    about.title('About')
    about.geometry('220x220')

    introduction=Label(about,text='Feather Note II\nby JackHou2005\n2019.8.26 updata',bg='#FFFFE0',width=30,height=30)
    introduction.pack(side=TOP)

top=Tk()
top.title('Feather Note II')
top.geometry('880x640')

clock=Label(top,text='Clock:'+time.strftime('%Y-%m-%d, %I(%H):%M:%S, %A',time.localtime(time.time())))
clock.after(1000,trickit)
clock.pack(side=BOTTOM)

summary=ScrolledText(top,width=100,height=8,bg='#FFFFE0')
summary.pack(side=BOTTOM,expand=True,fill=X)

summary_title=Label(top,text='Summary',bg='#FFFFE0',width=90,height=1)#There background was #F0E68C
summary_title.pack(side=BOTTOM,expand=True,fill=X)

evidence=Frame(top)
evidence.pack(side=LEFT,expand=False)

frame_t=Frame(evidence)
frame_b=Frame(evidence)
frame_t.pack(side=TOP,expand=True,fill=X)
frame_b.pack(side=BOTTOM,expand=True,fill=Y)

Label(frame_t,text='Evidence',bg='#FFFFE0',width=22,height=1).pack()#Ditto
notes=ScrolledText(frame_b,width=20,height=100,bg='#FFFFE0')
notes.pack(side=BOTTOM,expand=True,fill=Y)

contents=ScrolledText(top,width=80,height=20,bg='#FFFFE0')
contents.pack(side=BOTTOM,expand=True,fill=BOTH)

Btn=Btn_def()

menubar=Menu(top)
filemenu=Menu(menubar,tearoff=0)
menubar.add_cascade(label='File',menu=filemenu)
filemenu.add_command(label='Open',command=lambda:Btn.load(contents,notes,summary))
filemenu.add_command(label='Save',command=lambda:Btn.save(contents,notes,summary))
filemenu.add_command(label='Save As',command=lambda:Btn.save_as(contents,notes,summary))
aboutmenu=Menu(menubar,tearoff=0)
menubar.add_cascade(label='About',menu=aboutmenu)
aboutmenu.add_command(label='About',command=about)

top.config(menu=menubar)
top.mainloop()
